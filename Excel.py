import openpyxl


def write2xlsx(path, sheetname, value):
    index = len(value)
    workbook = openpyxl.Workbook()  # 实例化
    sheet = workbook.active  # 激活worksheet
    sheet.title = sheetname
    for i in range(index):
        for j in range(len(value[i])):
            sheet.cell(row=i + 1, column=j + 1, value=str(value[i][j]))
    workbook.save(path)
    print("xlsx格式表格数据写入成功！")


def append2xlsx(path, sheetname, value):
    index = len(value)
    workbook = openpyxl.load_workbook(path)
    sheet = workbook[sheetname]

    for i in range(index):
        sheet.append(list(value[i]))  # append的内容必须是可迭代对象，里面的value必须是str类型
    workbook.save(path)
    print("xlsx格式表格数据追加成功！")


if __name__ == '__main__':
    write2xlsx(path='./zx_test_2.xlsx', sheetname='text', value=[[1, 2, 3], [4, 5, 6]])
