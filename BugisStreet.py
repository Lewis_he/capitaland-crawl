import requests
from bs4 import BeautifulSoup
import re
from Excel import *

headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'}

class BugisStreet(object):
    def parse(self, url):
        values = []
        resp = requests.get(url, headers=headers)
        soup = BeautifulSoup(resp.text, 'lxml')
        # values = soup.find("a", string="BEAUTY & WELLNESS STORES")
        sections = soup.find_all("section", class_="cm cm-accordion section")
        for section in sections:
            category = section.find("a").text.strip()
            trs = section.find("table").find_all("tr")
            # for index in range(len(trs)):
            for tr in trs[1:]:
                tds = tr.find_all("td")
                for td in tds:
                    Brand = tds[0].getText()
                    store_code = tds[1].getText()
                    subCategory = tds[2].getText()
                value = ['Bugis Street', Brand, store_code, category, subCategory, '', '', '', '',
                         '', '', '', '', '', '']  # 因为Excel的数据插入要求所以将一行数据组成一个数组，并且添加到列表，
                values.append(value)

        return values

    def run(self):
        url = 'https://www.capitaland.com/sg/malls/bugis-street/en/stores.html'
        values = self.parse(url)
        append2xlsx('capital merchants data.xlsx', sheetname='Bugis Street', value=values)  # 调用插入Excel的API


if __name__ == '__main__':
    c = BugisStreet()
    c.run()