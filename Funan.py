import requests
from bs4 import BeautifulSoup
import re
from Excel import *

headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'}


class Funan(object):

    def parse_list(self, properties):
        # values = [['Mall name', 'Brand', 'store code', 'category', 'sub-catagory', 'openning hours', 'contact number',
        #            'WeChat Pay', 'AliPay', 'GrabPay', 'eCapitaVoucher', 'CapitaVoucher', 'CapitaCard', 'STAR$XTRA',
        #            'mechant description']]
        values = []
        for p in properties:
            Brand = p.get('jcr:title')
            url = p.get('pagePath')
            acceptsCapitacard = ('Y' if p.get('acceptsCapitacard').endswith('yes') else 'N') if p.get(
                'acceptsCapitacard') else 'N'
            acceptsCapitavoucher = ('Y' if p.get('acceptsCapitavoucher').endswith('yes') else 'N') if p.get(
                'acceptsCapitavoucher') else 'N'
            acceptseCapitavoucher = ('Y' if p.get('acceptseCapitavoucher').endswith('yes') else 'N') if p.get(
                'acceptseCapitavoucher') else 'N'
            # marketingCategory = p.get('marketingcategory')[0] if p.get(
            #     'marketingcategory') else 'capitalanddatabasemanagement:sg/tenant-categories/unknown/unknown'
            if p.get('marketingcategory'):
                marketingCategory = p.get('marketingcategory')[0]
                categoryStr = marketingCategory.replace('capitalanddatabasemanagement:sg/tenant-categories/', '', 1)
                if '/' in categoryStr:
                    category = categoryStr.split('/')[0]
                    subCategory = categoryStr.split('/')[1]
                else:
                    category = categoryStr
                    subCategory = ''
            else:
                category = ''
                subCategory = ''

            mechantDescription = p.get('shortdescription') if p.get('shortdescription') else ''
            store_code, contact_number, star_xtra, opening_hour = self.parse(url)
            value = ['Funan', Brand, store_code, category, subCategory, opening_hour, contact_number, '', '',
                     '',
                     acceptseCapitavoucher, acceptsCapitavoucher, acceptsCapitacard, star_xtra,
                     mechantDescription]  # 因为Excel的数据插入要求所以将一行数据组成一个数组，并且添加到列表，
            values.append(value)
        return values

    def parse(self, url):
        resp = requests.get(url, headers=headers)
        soup = BeautifulSoup(resp.text, 'lxml')
        store_code = soup.find(class_='icon-marker icon-rounded').text.strip() if soup.find(
            class_='icon-marker icon-rounded') else ''
        contact_number = soup.find(class_='icon-phone icon-rounded').text.strip() if soup.find(
            class_='icon-phone icon-rounded') else ''
        star_xtra = 'Y' if soup.find(class_='xtra-star-rewards') else 'N'
        opening_hour = soup.find(class_='starttime').text.strip() + ' to ' + soup.find(
            class_='endtime').text.strip() if soup.find(
            class_='opening-times icon-clock') else ''
        return store_code, contact_number, star_xtra, opening_hour

    def run(self):
        url = 'https://www.capitaland.com/apis/sg/en/properties/funan_1640709664/cl%3Aentity/tenants/cl%3Arelated-page/%2Fcontent%2Fcapitaland%2Fsg%2Fmalls%2Ffunan%2Fen%2Fstores/cl%3Asortby/jcr%3Atitle/asc/cl%3Aselectors/_rel_brandtenants_details/_rel_deals/_rel_properties_details/_rel_tenants_details/accepts/acceptsCapita3Eats/acceptsCapitacard/acceptsCapitavoucher/acceptschope/acceptseCapitavoucher/addressroadname/assettype/brand/capita3EatsLink/chopelink/city/country/countryCode/cq%3Atags/currency/dealExisted/enddate/endtime/entityType/entityname/firstPublished/jcr%3Atitle/listingTypePages/logoImgPath/malllocationnote/marketingcategory/nearesttrainstation/oldprice/pagePath/pageTitle/price/promotiontype/ribbon/ribboncolor/shortdescription/startdate/starttime/state/subtitle/thumbnail/tileColorScheme/tilesubtext/cl%3Apgcursor/16.json'
        resp = requests.get(url, headers=headers)
        properties = resp.json().get('properties')  # 获取整个json列表
        values = self.parse_list(properties)  # 获取所有需要插入形成一个数组
        append2xlsx('capital merchants data 202102.xlsx', sheetname='Funan', value=values)  # 调用插入Excel的API，


if __name__ == '__main__':
    c = Funan()
    c.run()
